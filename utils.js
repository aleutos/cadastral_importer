//changes unknow character for 'ñ' this is necesary due to the xml encode system being ISO-8859-1
exports.fixURL = url => encodeURI(url).replace(/%EF%BF%BD/g, "%C3%B1");

exports.toArray = array => Array.isArray(array) ? array : [array];

exports.requireIfExists = id => {
  try {
    return require(id);
  } catch (e) {
    console.log('Could not require', id);
    return;
  }
};
