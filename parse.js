// Functions to handle and parse external service (Cadastre) formats

const fs = require('fs');
const xml2js = require('xml2js');
const StreamZip = require('node-stream-zip');
const request = require('request-promise');

const { fixURL, toArray } = require('./utils');
const config = require('./config/config');

const parser = new xml2js.Parser({ explicitArray: false });
const builder = new xml2js.Builder({ headless: true });

const ALPHABET = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

const parseXML = (xml) => new Promise((res, rej) => {
  parser.parseString(xml, (err, result) => {
    if (err) rej(err);
    else res(result);
  });
});

const parseJson = (json) => {
  return builder.buildObject(json);
};

exports.parseProvinces = async (provincesData) => {
  const provincesXml = await parseXML(provincesData);
  return toArray(provincesXml.feed.entry).map(p => ({
    id: Number(p.title.substring(19, 21)),
    name: p.title.substring(22),
    url: p.id,
  }));
};

exports.fetchMunicipalities = async (province, targetProvinces, municipalityStart, municipalityEnd) => {
  const provinceData = await parseXML(await request(province.url));
  const municipalities = [];
  for (const entry of toArray(provinceData.feed.entry)) {
    const rawText = entry.title;
    const municipalityNum = Number(rawText.substring(0, rawText.indexOf('-')).trim().substring(2));
    if (province.id === targetProvinces[0].id) {
      if ((municipalityStart === -1 && municipalityEnd === -1)
        || (municipalityNum >= municipalityStart && municipalityNum <= municipalityEnd)) {
        const cadastreURL = fixURL(entry.id);
        const municipality = { provinceNum: province.id, municipalityNum, cadastreURL };
        municipalities.push(municipality);
      }
    } else {
      const cadastreURL = fixURL(entry.id);
      const municipality = { provinceNum: province.id, municipalityNum, cadastreURL };
      municipalities.push(municipality);
    }
  }
  return municipalities.sort((m1, m2) => m1.municipalityNum - m2.municipalityNum);
};

exports.fetchPolygons = async (file, polygonStart, polygonEnd, firstMunicipality) => {
  let polygons = [];
  const polygonsInfo = await parseXML(fs.readFileSync(file, 'utf8'));
  for (const member of toArray(polygonsInfo.FeatureCollection.member)) {
    const polygonNumber = Number(member['cp:CadastralZoning']['cp:label']);
    if (member['cp:CadastralZoning']['cp:levelName']['gmd:LocalisedCharacterString']["_"].includes('POLIGONO')) {
      if (firstMunicipality) {
        if ((polygonStart === -1 && polygonEnd === -1) || (polygonNumber >= polygonStart && polygonNumber <= polygonEnd)) {
          let polygonRef = member['cp:CadastralZoning']['cp:inspireId']['Identifier']['localId'];
          if (isNaN(polygonRef.charAt(5)) && !isNaN(polygonRef.charAt(4)) && !isNaN(polygonRef.charAt(6))) {
            const sector = ALPHABET.indexOf(polygonRef.charAt(5));
            const json = member['cp:CadastralZoning']['cp:geometry'];
            json['gml:MultiSurface']['$'] = {
              srsName: 'EPSG:' + json['gml:MultiSurface']['$']['srsName'].substring(
                json['gml:MultiSurface']['$']['srsName'].lastIndexOf('/') + 1,
                json['gml:MultiSurface']['$']['srsName'].length)
            };
            for (let surface of toArray(json['gml:MultiSurface']['gml:surfaceMember'])) {
              surface['gml:Surface']['$'] = {
                srsName: 'EPSG:' + surface['gml:Surface']['$']['srsName'].substring(
                  surface['gml:Surface']['$']['srsName'].lastIndexOf('/') + 1,
                  surface['gml:Surface']['$']['srsName'].length)
              };
            }
            const geometry = parseJson(json);
            polygons.push({ polygonNumber, polygonRef, sector, geometry });
          }
        }
      } else {
        const polygonRef = member['cp:CadastralZoning']['cp:inspireId']['Identifier']['localId'];
        if (isNaN(polygonRef.charAt(5)) && !isNaN(polygonRef.charAt(4)) && !isNaN(polygonRef.charAt(6))) {
          const sector = ALPHABET.indexOf(polygonRef.charAt(5));
          const json = member['cp:CadastralZoning']['cp:geometry'];
          json['gml:MultiSurface']['$'] = {
            srsName: 'EPSG:' + json['gml:MultiSurface']['$']['srsName'].substring(
              json['gml:MultiSurface']['$']['srsName'].lastIndexOf('/') + 1,
              json['gml:MultiSurface']['$']['srsName'].length)
          };
          for (let surface of toArray(json['gml:MultiSurface']['gml:surfaceMember'])) {
            surface['gml:Surface']['$'] = {
              srsName: 'EPSG:' + surface['gml:Surface']['$']['srsName'].substring(
                surface['gml:Surface']['$']['srsName'].lastIndexOf('/') + 1,
                surface['gml:Surface']['$']['srsName'].length)
            };
          }
          const geometry = parseJson(json);
          polygons.push({ polygonNumber, polygonRef, sector, geometry });
        }
      }
    }
  }
  return polygons;
};

exports.fetchParcels = async (file) => {
  let parcels = [];
  const parcelsInfo = await parseXML(fs.readFileSync(file, 'utf8'));
  for (let member of toArray(parcelsInfo.FeatureCollection.member)) {
    let cadastralRef = member['cp:CadastralParcel']['cp:inspireId']['Identifier']['localId'];
    if (isNaN(cadastralRef.charAt(5)) && !isNaN(cadastralRef.charAt(4)) && !isNaN(cadastralRef.charAt(6))) {
      const parcelNumber = Number(cadastralRef.substr(9));
      // Ignore special infrastructure parcels over 9000
      if (parcelNumber >= 9000) continue;
      let json = member['cp:CadastralParcel']['cp:geometry'];
      json['gml:MultiSurface']['$'] = {
        srsName: 'EPSG:' + json['gml:MultiSurface']['$']['srsName'].substring(
          json['gml:MultiSurface']['$']['srsName'].lastIndexOf('/') + 1,
          json['gml:MultiSurface']['$']['srsName'].length)
      };
      for (let surface of toArray(json['gml:MultiSurface']['gml:surfaceMember'])) {
        surface['gml:Surface']['$'] = {
          srsName: 'EPSG:' + surface['gml:Surface']['$']['srsName'].substring(
            surface['gml:Surface']['$']['srsName'].lastIndexOf('/') + 1,
            surface['gml:Surface']['$']['srsName'].length)
        };
      }
      let geometry = parseJson(json);
      parcels.push({ parcelNumber, cadastralRef, geometry });
    }
  }
  return parcels;
};

exports.extractFiles = (cadastreURL) => {
  return new Promise((res, rej) => {
    if (!fs.existsSync(config.tmpDir)) {
      fs.mkdirSync(config.tmpDir);
    }
    const file = fs.createWriteStream(`${config.tmpDir}/cadastral_parcels.zip`);
    const pipe = request(cadastreURL).pipe(file);
    pipe.on('error', err => {
      console.log("URL gave no zip file for url " + cadastreURL, err);
      rej(err);
    });
    pipe.on('finish', () => {
      file.close();
      const zip = new StreamZip({
        file: `${config.tmpDir}/cadastral_parcels.zip`,
        storeEntries: true
      });
      zip.on('error', err => {
        console.log("Error extracting zip for url " + cadastreURL, err);
        zip.close();
        rej(err);
      });
      zip.on('ready', () => {
        fs.mkdirSync(`${config.tmpDir}/extracted`);
        zip.extract(null, `${config.tmpDir}/extracted`, err => {
          zip.close();
          if (err) {
            console.log("Error extracting zip for url " + cadastreURL, err);
            rej(err);
          } else {
            res(Object.values(zip.entries()).map(r => `${config.tmpDir}/extracted/` + r.name));
          }
        });
      });
    });
  });
};
