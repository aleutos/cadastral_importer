const os = require('os');

module.exports = {
  baseUrl: 'http://www.catastro.minhap.es/INSPIRE/CadastralParcels',
  tmpDir: `${os.tmpdir()}/cadastre_importer`,
  db: {
    hostname: 'host.name',
    port: 5432,
    username: "smartwood_user",
    password: "smartwood_pass",
    database: "smartwood_db"
  }
};