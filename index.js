
const fs = require('fs');
const path = require('path');
const readline = require('readline');
const cleanup = require('node-cleanup');
const request = require('request-promise');
const { omit, noop, pick } = require('lodash');
const {
  insertCadastrePolygon, updateCadastrePolygon, findCadastrePolygons, findPolygonParcels,
  updateCadastrePolygonBounds, updateMunicipalityBounds, pgPool, insertMunicipality,
  insertParcel, updateParcel, removeOldParcels
} = require('./db');
const {
  parseProvinces, fetchMunicipalities, fetchPolygons, fetchParcels, extractFiles
} = require('./parse');

const config = require('./config/config');
const { baseUrl } = require('./config/config');

let currentMunicipality;
let currentProvince;
let currentPolygon;

if (fs.existsSync(path.join(config.tmpDir, 'extracted'))) {
  console.error('Folder for extract files already exists, please remove it and run the script again', path.join(config.tmpDir, 'extracted'));
  process.exit();
}

cleanup((exitCode, signal) => {
  console.log("Exit", { exitCode, signal });
  if (signal) {
    fs.rmdirSync(config.tmpDir, { recursive: true });
    saveProgress();
    console.log("\n");
    console.log("Last inserted Province " + currentProvince);
    console.log("Last inserted Municipality " + currentMunicipality);
    console.log("Last inserted Polygon " + currentPolygon);
    pgPool.end().then(() => process.kill(process.pid, signal));
    cleanup.uninstall();
    return false;
  }
});

const saveProgress = () => {
  const status = { currentProvince, currentMunicipality, currentPolygon };
  fs.writeFileSync("continue.json", JSON.stringify(status, null, 2));
};

let municipalityStart = -1;
let municipalityEnd = -1;
let polygonStart = -1;
let polygonEnd = -1;
const parseArguments = async () => {
  const args = process.argv.slice(2);

  let targetProvinces = await parseProvinces(
    await request(`${baseUrl}/ES.SDGC.CP.atom.xml`)
  );

  if (args.indexOf('--provinces') > -1) {
    const range = args[args.indexOf('--provinces') + 1].split('-');
    targetProvinces = targetProvinces.filter(p => (Number(p.id) >= Number(range[0]) && Number(p.id) <= Number(range[1])));
  } else if (args.indexOf('--province') > -1) {
    const provinceCode = args[args.indexOf('--province') + 1];
    targetProvinces = targetProvinces.filter(p => Number(p.id) === Number(provinceCode));
  }

  if (args.indexOf('--municipalities') > -1) {
    const range = args[args.indexOf('--municipalities') + 1].split('-');
    municipalityStart = Number(range[0]);
    municipalityEnd = Number(range[1]);
  } else if (args.indexOf('--municipality') > -1) {
    municipalityStart = Number(args[args.indexOf('--municipality') + 1]);
    municipalityEnd = Number(args[args.indexOf('--municipality') + 1]);
  }

  if (args.indexOf('--polygons') > -1) {
    const range = args[args.indexOf('--polygons') + 1].split('-');
    polygonStart = Number(range[0]);
    polygonEnd = Number(range[1]);
  } else if (args.indexOf('--polygon') > -1) {
    polygonStart = Number(args[args.indexOf('--polygon') + 1]);
    polygonEnd = Number(args[args.indexOf('--polygon') + 1]);
  }

  if (args.indexOf('--continue') > -1 && fs.existsSync('./continue.json')) {
    let continueJson = fs.readFileSync('./continue.json', { encoding: 'utf8', flag: 'r' });

    continueJson = JSON.parse(continueJson);
    currentProvince = continueJson.currentProvince;
    currentMunicipality = continueJson.currentMunicipality;
    currentPolygon = continueJson.currentPolygon;
  }

  if (currentProvince) {
    targetProvinces = targetProvinces.filter(r => r.id >= currentProvince);
  }
  if (currentMunicipality) {
    municipalityStart = Number(currentMunicipality);
    municipalityEnd = 999;
  } else {
    municipalityStart = -1;
    municipalityEnd = -1;
  }
  if (currentPolygon) {
    polygonStart = Number(currentPolygon);
    polygonEnd = 9999;
  } else {
    polygonStart = -1;
    polygonEnd = -1;
  }

  targetProvinces.sort((p1, p2) => p1.id - p2.id);
  return targetProvinces;

};

(async () => {
  let pendingBoundsUpd = Promise.resolve();
  const targetProvinces = await parseArguments();
  if (!targetProvinces) { console.log("No provinces to process"); }
  console.log('Starting job at', { provinceStart: targetProvinces[0].id, municipalityStart, polygonStart });

  try {
    await pgPool.connect();
    for (const province of targetProvinces) {
      currentProvince = province.id;

      console.log('Fetching municipalities for: ' + province.name);
      const municipalities = await fetchMunicipalities(province, targetProvinces, municipalityStart, municipalityEnd);

      for (const municipality of municipalities) {
        console.log("Processing municipality", municipality);
        currentMunicipality = municipality.municipalityNum;

        const municipRes = await insertMunicipality(province.id, municipality.municipalityNum);
        const municipalityId = municipRes.id;
        console.log(`${municipRes.inserted ? 'Inserted' : 'Existing'} Municipality with id: ${municipalityId}`);

        console.log('Downloading Cadastre data from ' + municipality.cadastreURL);
        const files = await extractFiles(municipality.cadastreURL);
        console.log('Succesfully extracted: ', files);

        const polygons = await fetchPolygons(
          files.find(r => r.includes('cadastralzoning')),
          polygonStart, polygonEnd,
          currentMunicipality === municipalityStart
        );
        const parcels = await fetchParcels(
          files.find(r => r.includes('cadastralparcel'))
        );

        const existingPolygons = await findCadastrePolygons(municipalityId);
        for (const polygon of polygons) {
          currentPolygon = polygon.polygonNumber;
          try {
            const polygonParcels = parcels.filter(r => r.cadastralRef.startsWith(polygon.polygonRef));
            if (polygonParcels.length === 0) {
              console.log('Empty polygon (skip):' + polygon.polygonRef);
              continue;
            }

            const existingPolygon = existingPolygons.find(p =>
              p.polygon_num === polygon.polygonNumber && p.sector === polygon.sector
            );
            let polygonId;
            if (existingPolygon) {
              polygonId = existingPolygon.id;
              await updateCadastrePolygon(polygonId, polygon);
              process.stdout.write('Polygon updated: ');
            } else {
              polygonId = await insertCadastrePolygon(municipalityId, polygon);
              process.stdout.write('Polygon inserted');
              existingPolygons.push({
                id: polygonId,
                polygon_num: polygon.polygonNumber,
                municipality_id: municipalityId,
                sector: polygon.sector,
              });
            }
            console.log(`${polygon.polygonNumber} (${polygonId})`);

            let insertedPCount = 0;
            let updatedPCount = 0;
            let errorPCount = 0;
            const existingParcels = await findPolygonParcels(polygonId);
            await removeOldParcels(existingParcels, polygonParcels);

            await pgPool.query("BEGIN");
            for (const polygonParcel of polygonParcels) {
              const existingParcel = existingParcels.find(p => p.parcel_num === polygonParcel.parcelNumber);
              try {
                await pgPool.query("SAVEPOINT insertParcel");
                if (existingParcel) {
                  await updateParcel(existingParcel.id, polygonParcel);
                  updatedPCount++;
                } else {
                  await insertParcel(polygonId, polygonParcel);
                  insertedPCount++;
                }
                logNewParcel(!!existingParcel, polygonParcel.cadastralRef, insertedPCount, updatedPCount, errorPCount);
                await pgPool.query("RELEASE SAVEPOINT insertParcel");
              } catch (err) {
                const parcelId = existingParcel && existingParcel.id;
                fs.appendFile('error.log', JSON.stringify({
                  time: new Date(),
                  parcel: {
                    parcelId,
                    polygonId,
                    ...pick(polygonParcel, 'cadastralRef', 'geometry')
                  },
                  error: err.message,
                }, null, 2) + "\n", noop);
                errorPCount++;
                console.log("\n");
                console.error(`Parcel: ${polygonParcel.cadastralRef} gave error: ${err.message}`);
                console.error("Parcel data:", {
                  parcel: omit(polygonParcel, 'geometry'),
                  parcelId,
                  polygon: omit(polygon, 'geometry'),
                  polygonId,
                });
                console.error(err);
                await pgPool.query("ROLLBACK TO SAVEPOINT insertParcel");
              }
            }
            console.log(`\nRecalculating bounds for polygon ${polygonId} (${polygon.polygonRef})`);
            await updateCadastrePolygonBounds(polygonId);
            await pgPool.query("COMMIT");
            console.log("End inserting data for polygon " + polygon.polygonNumber);
          } catch (err) {
            pgPool.query("ROLLBACK");
            throw err;
          }
        }
        console.log(`Recalculating bounds for municipality ${municipalityId} (${municipality.municipalityNum})`);
        await pendingBoundsUpd;
        pendingBoundsUpd = updateMunicipalityBounds(municipalityId);
        fs.rmdirSync(config.tmpDir, { recursive: true });
        console.log('Files succesfully deleted');
        currentPolygon = -1;
      }
      console.log('End with province ' + province.name);
      currentMunicipality = -1;
      saveProgress();
    }
    await pendingBoundsUpd;
    await pgPool.end();
    if (fs.existsSync(config.tmpDir)) {
      fs.rmdirSync(config.tmpDir);
    }
  } catch (err) {
    console.error(err);
    cont = { "currentMunicipality": currentMunicipality, "currentProvince": currentProvince, "currentPolygon": currentPolygon };
    fs.writeFileSync("continue.json", JSON.stringify(cont, null, 2));
    console.log("Stopped at", { currentMunicipality, currentProvince, currentPolygon });

    await pgPool.end();
  }
})().catch(e => {
  console.error(e);
});

const logNewParcel = (updated, cadastralRef, insertedCount, updatedCount, errorCount) => {
  if (process.stdout.isTTY) {
    readline.clearLine(process.stdout, 0);
    readline.cursorTo(process.stdout, 0);
    process.stdout.write(`${updated ? 'Updated' : 'Inserted'} Parcel: ${cadastralRef},`);
    process.stdout.write(` inserted: ${insertedCount},`);
    process.stdout.write(` Updated: ${updatedCount},`);
    process.stdout.write(` with errors: ${errorCount}`);
  } else {
    process.stdout.write(`${updated ? 'U' : '+'} ${cadastralRef} `);
  }
};
