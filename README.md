# SmartWood service importer

Service to import data from cadastral to the SmartWood database. A stand-alone process that can be used to update the parcels, cadastre_polygons and municipalities tables as they are stored for the SaaS SmartWood.

Feel free to adapt it to your needs, or get inspired by it as long you comply its license.

## Configuration

1. Create a file named config.js in the config folder based on the template supplied in config.example.js.

2. Leave the baseUrl as it is and put the information of your database under the db section, you can leave the tempDir as it is or give it a path of your choice.

3. Modify the queries in the insert functions in index.js if necessary according to your database schema.

## Running

1. Install [node.js](https://nodejs.org) if you dont have it already.

2. Run this command in the root directory of the project to install all the necessary packages (this is also needed when updating the project): 
  ```bash
  $ npm install 
  ```

3. Run the index.js file, using the command:
  ```bash
  $ node index.js
  ```
These are the optional parameters for the script:
  - `--province provinceId` to specify a single province `--provinces startingProvincesId-endingProvinceId` to specify a range of provinces 
  - `--municipality municipalityNumber` to specify a single municipality `--municipalities startingMunitipalityNumber-endingMunicipalityNumber` to specify a range of municipalities
  - `--polygon polygonNumber` to specify a single polygon `--polygons startingPolygonNumber-endingPolygonNumber` to specify a range of polygons
