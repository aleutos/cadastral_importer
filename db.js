const { Client } = require('pg');
const config = require('./config/config');

const pgPool = exports.pgPool = new Client({
  user: config.db.username,
  host: config.db.hostname,
  database: config.db.database,
  password: config.db.password,
  port: config.db.port,
});

const sqlGeometryTransformation = exports.sqlGeometryTransformation = (gml) => `st_transform(st_geomfromgml(${gml}),4326)`;

exports.insertMunicipality = async (province_num, number) => {
  const existing = await pgPool.query('SELECT id from municipalities WHERE province_num = $1 AND number = $2', [province_num, number]);
  if (existing.rows.length) return { id: existing.rows[0].id, inserted: false };
  const result = await pgPool.query('INSERT INTO municipalities(province_num,number) VALUES ($1,$2) RETURNING id', [province_num, number]);
  return { id: Number(result.rows[0].id), inserted: true };
};

exports.findCadastrePolygons = async (municipalityId) => {
  const result = await pgPool.query(
    'SELECT id, polygon_num, municipality_id, sector FROM cadastre_polygons WHERE municipality_id = $1',
    [municipalityId]);
  return result.rows.map(r => ({
    ...r,
    id: Number(r.id),
    polygon_num: Number(r.polygon_num),
    sector: Number(r.sector)
  }));
};

exports.insertCadastrePolygon = async (municipalityId, polygon) => {
  const geometrySql = sqlGeometryTransformation('$4');
  const response = await pgPool.query(`
      INSERT INTO cadastre_polygons(municipality_id, polygon_num, sector, geometry) 
      VALUES ($1, $2, $3, ${geometrySql})
      RETURNING id`,
    [municipalityId, polygon.polygonNumber, polygon.sector, polygon.geometry]);
  return Number(response.rows[0].id);
};

exports.updateCadastrePolygon = (polygonId, polygon) => {
  const geometrySql = sqlGeometryTransformation('$2');
  return pgPool.query(`
    UPDATE cadastre_polygons SET
      geometry = ${geometrySql}
    WHERE id = $1`,
    [polygonId, polygon.geometry]
  );
};

exports.findPolygonParcels = async (polygonId) => {
  const result = await pgPool.query(
    'SELECT id, cadastral_ref, polygon_id, parcel_num FROM parcels WHERE polygon_id = $1',
    [polygonId]);
  return result.rows.map(r => ({
    ...r,
    id: Number(r.id),
    polygon_id: Number(r.polygon_id),
    parcel_num: Number(r.parcel_num),
  }));
};

exports.insertParcel = (polygonId, parcel) =>
  pgPool.query(`
    INSERT INTO parcels (cadastral_ref,polygon_id, parcel_num, geometry, inserted_at, updated_at) 
    VALUES ($1, $2, $3, ${sqlGeometryTransformation('$4')}, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)`,
    [parcel.cadastralRef, polygonId, parcel.parcelNumber, parcel.geometry]);


exports.updateParcel = (parcelId, parcel) =>
  pgPool.query(`
    UPDATE parcels SET 
      geometry = ${sqlGeometryTransformation('$2')},
      cadastral_ref = $3, 
      updated_at = CURRENT_TIMESTAMP
    WHERE id = $1`,
    [parcelId, parcel.geometry, parcel.cadastralRef]);

exports.removeOldParcels = async (dbExistingParcels, newParcels) => {
  const newParcelNumbers = newParcels.map(p => p.parcelNumber);
  const toRemove = dbExistingParcels
    .filter(p => !newParcelNumbers.includes(p.parcel_num));
  const deleted = [];
  await pgPool.query('BEGIN');
  for (const p of toRemove) {
    try {
      // Ignore if referenced
      await pgPool.query('SAVEPOINT deleteparcel');
      await pgPool.query('DELETE FROM parcels WHERE id = $1', [p.id]);
      await pgPool.query('RELEASE SAVEPOINT deleteparcel');
      deleted.push(p.cadastral_ref);
    } catch (e) {
      await pgPool.query('ROLLBACK TO SAVEPOINT deleteparcel');
    }
  }
  await pgPool.query('COMMIT');
  if (deleted.length)
    console.log('Removed missing parcels: ' + deleted.join(', '));
};

exports.updateCadastrePolygonBounds = (polygonId) =>
  pgPool.query(`
    WITH polygon_bounds as (
      SELECT 
        polygon_id,
        min(st_ymin(geometry)) min_latitude,
        max(st_ymax(geometry)) max_latitude,
        min(st_xmin(geometry)) min_longitude,
        max(st_xmax(geometry)) max_longitude
      FROM parcels p WHERE polygon_id = $1
      GROUP BY polygon_id
    )
    UPDATE cadastre_polygons pol SET
      min_latitude = bounds.min_latitude,
      max_latitude = bounds.max_latitude,
      min_longitude = bounds.min_longitude,
      max_longitude = bounds.max_longitude
    FROM polygon_bounds bounds
    WHERE bounds.polygon_id = pol.id`,
    [polygonId]);


exports.updateMunicipalityBounds = (municipalityId) =>
  pgPool.query(`
    WITH municipality_bounds as (
      SELECT
        municipality_id,
        min(min_latitude) "min_latitude",
        max(max_latitude) "max_latitude",
        min(min_longitude) "min_longitude",
        max(max_longitude) "max_longitude"
      FROM cadastre_polygons cp
      WHERE cp.municipality_id = $1
      GROUP BY municipality_id
    )
    UPDATE municipalities mun SET
      min_latitude = bounds.min_latitude,
      max_latitude = bounds.max_latitude,
      min_longitude = bounds.min_longitude,
      max_longitude = bounds.max_longitude
    FROM municipality_bounds bounds
    WHERE bounds.municipality_id = mun.id`,
    [municipalityId]);